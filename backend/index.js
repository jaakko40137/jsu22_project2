const express = require("express");
path = require("path");
const app = express(),
  port = process.env.PORT || 3000;

const dotenv = require("dotenv");
({ Client } = require("pg"));

dotenv.config();

const client = new Client({
  connectionString: process.env.PGURI,
});

client.connect();

app.use(express.urlencoded({ extended: false }));

app.get("/api", async (_req, res) => {
  const { rows } = await client.query("SELECT * FROM confdates");

  res.send(rows);
});

app.use(express.static(path.join(path.resolve(), "dist")));

app.listen(port, () => {
  console.log(`Redo på http://localhost:${port}/`);
});
